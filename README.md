# Fork of Legate.Core

See https://github.com/nv-legate/legate.core

# Notes on this repo

This repo mashes together legate and legion and breaks a lot of the generalities of legate. The purpose of this repo is therefore to act as a proof of concept for implementing a jupyter kernel inside of legion/legate. 

## Legate changes

The `install.py` script has been mangled in such a way that this repo can just be clone and build on the test cluster (Darwin) I was using. 

## Legion changes 

The `legion_top.py` script was changed so that by default if no script is handed to it, it will start the jupyter kernel. I did not have the time to put together the logic to have jupyter be a stand alone flag and instead focused on putting together a proof of concept. 

The `main.cc` file was changed to just assume control replication always. Again this was a time constrained decision to focus on a proof of concept.

## Install procedure

This is the install procedure I took to install on Darwin

```bash
salloc -p clx-volta -N2 
module load miniconda cuda/11.0 openmpi/4.0.3-gcc_9.3.0
conda create -n legate-jup-env pip 
source activate legate-jup-env
conda install pyarrow=1.0.1 numpy cffi jupyter matplotlib

./install.py --gasnet --conduit ibv --cuda --with-cuda $CUDA_HOME --arch volta
```

A `kernel.json` file needs to be manually installed (need to figure out how best to get this working). Typically the kernel file goes in a users `~/.local/share/jupyter/kernels` directory (can double check with `jupyter --paths`) so create a `kernel.json` file in `~/.local/share/jupyter/kernels/legate_jup/` with

```json
{
  "argv": ["install/path/to/legate", "list", "of", "legate", "args", "--jupyter-connection-file", "{connection_file}"], 
  "display_name": "LegateJupyter testing", 
  "language": "python"
}
```

This is not the ideal way to do this because it locks you into your legate args. Need to think this through more

Legate.numpy can then be installed pointing to this install. Need to use a legate.numpy on Darwin that doesn't use openmp when building openblas